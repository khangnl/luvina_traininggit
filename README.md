Test Conflict 2
Nguyen Linh Khang
--------------------------------------

DEV_Luvina Software JSC. 

Hoa Binh Tower, 106 Hoang Quoc Viet Road., Cau giay Dist., Hanoi, Vietnam. 

Email: nguyenlinhkhang@luvina.net

Tel: 0975181095

The information in this e-mail and any attached files is CONFIDENTIAL. It is intended solely for the addressee, or the employee or agent responsible for delivering such materials to the addressee.  If you have received this message in error please return it to the sender then delete the email and destroy any copies of it.  If you are not the intended recipient, any form of reproduction, dissemination, copying, disclosure, modification, distribution and/or publication or any action taken or omitted to be taken in reliance upon this message or its attachments is prohibited and may be unlawful.

At present the integrity of e-mail across the Internet cannot be guaranteed and messages sent via this medium are potentially at risk.  All liability is excluded to the extent permitted by law for any claims arising as a result of the use of this medium to transmit information by or to the LUVINA Software JSC.
